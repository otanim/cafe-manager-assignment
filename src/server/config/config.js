module.exports = {
  local: {
    db: 'mongodb://localhost:27017/cafeManagerAssignment'
  },

  development: {
    db: 'mongodb://localhost:27017/cafeManagerAssignment'
  },

  production: {
    db: 'mongodb://localhost:27017/cafeManagerAssignment'
  }
};
