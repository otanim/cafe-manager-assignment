# README #

Hi dear devs, I've tried to finish the whole application, but I did not have enough time for it, if you would like, I can finish it later. 

### How to start ###

```javascript
npm install
npm start
```

### Basic source guideline ###

* **src/app/** - Front end.
* **src/server/** - Back end.

### What was done ###

#### Manager

* **[B][F]** Create waiter.
* **[B][F]** Create table.
* **[B][F]** Create product.
* **[B]** Assign tables to waiter.

#### Waiter

* **[B]** See tables assigned to him.
* **[B]** Create order for table.
* **[B]** Create ProductInOrder for Order.
* **[B]** Edit ProductInOrder fields.
* **[B]** Edit order fields.

\***[B]** is Backend.  
\***[F]** is Frontend.

### P.S. ###
I've seen that this task was the modified version of [this project](https://github.com/alahijani/zprojects).
I think that's enough to have simple task, because when project becomes a little bit complex, arises demand to close all possible cases, which requires more time.
Also some of description is not clear to understand, for example:  
> 5. ProductInOrder - fields' definition is up to implementer

It's not clear what does this field for.

> Create user

As I understand User's type is Manager and Waiter. So it's not clear, that whom can Manager create, only Waiter, or Manager too ?

And also, if the Waiter was created, should he login via credential data, or what ?

### P.P.S. ###
If this status of commit was not enough to test my skills, please let me know (otanim617@gmail.com). I can finish it in upcoming weekend.